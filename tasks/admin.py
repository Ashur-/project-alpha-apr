from django.contrib import admin
from tasks.models import Task


# register models
admin.site.register(Task)
