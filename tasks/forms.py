from django import forms
from .models import Task


# Tasks forms v
class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed", "owner"]
