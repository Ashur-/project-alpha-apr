from django.contrib import admin
from projects.models import Project

# register models
admin.site.register(Project)
