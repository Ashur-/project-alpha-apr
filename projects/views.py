from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.db import models
from projects.forms import ProjectForm


@login_required
def project_list(request):
    projects = Project.objects.filter(
        models.Q(owner=request.user)
        | models.Q(
            members=request.user
        )  # hardest moment of my life xD dug deep into ORM
    ).distinct()
    context = {"projects": projects}
    return render(request, "project/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {
        "project": project,
        "tasks": tasks,
        "project_name": project.name,
    }
    return render(request, "project/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "project/create.html", {"form": form})
